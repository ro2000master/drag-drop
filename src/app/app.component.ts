import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    public boxes = ['box1', 'box2'];

    addBox() {
        this.boxes.push('added');
    }
}
