import {Directive, OnDestroy, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {DraggableDirective} from './draggable.directive';
import {GlobalPositionStrategy, Overlay, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';

@Directive({
    selector: '[appHelper]',
    exportAs: 'appHelper'
})
export class HelperDirective implements OnInit, OnDestroy {

    private overlayRef: OverlayRef;
    private positionStrategy = new GlobalPositionStrategy();
    private startPosition?: {x: number, y: number};

    constructor(private draggable: DraggableDirective,
                private templateRef: TemplateRef<any>,
                private viewContainerRef: ViewContainerRef,
                private overlay: Overlay) {
    }

    ngOnInit() {
        this.draggable.dragStart.subscribe((event) => this.onDragStart(event));
        this.draggable.dragMove.subscribe((event) => this.onDragMove(event));
        this.draggable.dragEnd.subscribe(() => this.onDragEnd());

        // CREATE OVERLAY
        this.overlayRef = this.overlay.create({
            positionStrategy: this.positionStrategy
        });
    }

    ngOnDestroy() {
        this.overlayRef.dispose();
    }

    private onDragStart(event: PointerEvent) {
        // SET START POSITION
        const clientRect = this.draggable.element.nativeElement.getBoundingClientRect();
        this.startPosition = {
            x: event.clientX - clientRect.left,
            y: event.clientY - clientRect.top
        };
    }

    private onDragMove(event: PointerEvent) {

        // RENDER HELPER
        if (!this.overlayRef.hasAttached()) {
            this.overlayRef.attach(new TemplatePortal(this.templateRef, this.viewContainerRef));
        }

        // POSITION HELPER
        this.positionStrategy.left(`${event.clientX - this.startPosition.x}px`);
        this.positionStrategy.top(`${event.clientY - this.startPosition.y}px`);
        this.positionStrategy.apply();
    }

    private onDragEnd() {
        // REMOVE HELPER
        // this.viewContainerRef.clear();
        this.overlayRef.detach();
    }

}
