import {
    ContentChild,
    Directive, ElementRef,
    EventEmitter,
    HostBinding,
    HostListener,
    Input,
    Output,
    TemplateRef,
    ViewContainerRef
} from '@angular/core';
import {HelperDirective} from './helper.directive';

@Directive({
    selector: '[appDraggable]'
})
export class DraggableDirective {

    // @ContentChild(HelperDirective) helper: HelperDirective;

    @HostBinding('class.draggable') draggable = true;
    @HostBinding('class.dragging') private dragging = false;

    @Output() dragStart = new EventEmitter<PointerEvent>();
    @Output() dragMove = new EventEmitter<PointerEvent>();
    @Output() dragEnd = new EventEmitter<PointerEvent>();

    constructor(public element: ElementRef) {}

    @HostListener('pointerdown', ['$event'])
    onPointerDown(event: PointerEvent): void {
        this.dragging = true;
        event.stopPropagation(); // For nested
        this.dragStart.emit(event);
    }

    @HostListener('document:pointermove', ['$event'])
    onPointerMove(event: PointerEvent): void {
        if (!this.dragging) {
            return;
        }
        this.dragMove.emit(event);
    }

    @HostListener('document:pointerup', ['$event'])
    onPointerUp(event: PointerEvent): void {
        if (!this.dragging) {
            return;
        }
        this.dragging = false;
        this.dragEnd.emit(event);
    }
}
