import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DraggableDirective} from './draggable.directive';
import {MovableDirective} from './movable.directive';
import {AreaDirective} from './area.directive';
import { HelperDirective } from './helper.directive';
import {OverlayModule} from '@angular/cdk/overlay';

@NgModule({
    declarations: [
        DraggableDirective,
        MovableDirective,
        AreaDirective,
        HelperDirective
    ],
    exports: [
        DraggableDirective,
        MovableDirective,
        AreaDirective,
        HelperDirective
    ],
    imports: [
        CommonModule,
        OverlayModule
    ]
})
export class DragAndDropModule {
}
